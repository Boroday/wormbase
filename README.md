# microPublication

This is the repo for the submission web application for micropublication.org.

The project was developed by the [Coko Foundation](https://coko.foundation/) using [Pubsweet](https://gitlab.coko.foundation/pubsweet/pubsweet).  
microPublication is published by [Caltech](http://www.caltech.edu/).

## Changelog

You can see the latest introduced features and fixes in our [changelog](https://gitlab.coko.foundation/micropubs/wormbase/blob/master/CHANGELOG.md).

## Roadmap

You can follow what we're working by taking a look at our [milestones](https://gitlab.coko.foundation/micropubs/wormbase/milestones), as well as our [issue tracker](https://gitlab.coko.foundation/micropubs/wormbase/issues).

<!---
| Task                      | In progress | Done     | Issue Number |
| ------------------------- | ----------- | -------- | ------------ |
| Set up login/signup       |             | &#x2714; | #27          |
| Docker container for db   |             | &#x2714; | #3           |
| Private routing           |             | &#x2714; | #2           |
| Implement Navbar          |             | &#x2714; | #14          |
| Setup Loaders             |             | &#x2714; | #28          |
| Linting                   |             | &#x2714; | #8           |
| Linting on Commit         |             | &#x2714; | #24          |
| Conventional Commits      |             | &#x2714; | #11          |
| Unit Tests Scripts        |             | &#x2714; | #7           |
| e2e Test Scripts          |             | &#x2714; | #6           |
| Decide on Form Lib        |             | &#x2714; | #18          |
| Set up Dashboard          |             | &#x2714; | #16          |
| Set up Form Submission    |             | &#x2714; | #17          |
| Amigo API extension       |             | &#x2714; | #21          |
| Wormbase DB API extension |             | &#x2714; | #19          |
| Roles & permissions       |             | &#x2714; | #19          |
| Granttome API extension   |             |          | #20          |
-->

## Installation

Please read our `INSTALL` file for installation instructions.
