const model = require('./externalUser')

module.exports = {
  model,
  modelName: 'ExternalUser',
}
