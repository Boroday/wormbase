/* eslint-disable sort-keys */

const addExternalReviewer = require('./addExternalReviewer/addExternalReviewer.resolver')
const teamsForArticle = require('./teamsForArticle/teamsForArticle.resolver')
const {
  ExternalTeam,
  getExternalTeamsForManuscript,
  inviteExternalReviewer,
  normalizeTeamMembership,
} = require('./externalTeam/externalTeam.resolver')
const {
  createManuscript,
  sendChat,
} = require('./manuscript/manuscript.resolver')

const resolvers = {
  Query: {
    getExternalTeamsForManuscript,
    teamsForArticle,
  },
  Mutation: {
    addExternalReviewer,
    createManuscript,
    inviteExternalReviewer,
    normalizeTeamMembership,
    sendChat,
  },
  ExternalTeam,
}

module.exports = resolvers
