import React from 'react'

import ArticlePreview from './ArticlePreview'
import { Modal, ModalHeader } from '../../ui'

import { formValuesToData } from '../formElements/helpers'

const Header = <ModalHeader text="article preview" />

const ArticlePreviewModal = props => {
  const { article, values, ...rest } = props
  if (!article && !values) return null

  return (
    <Modal headerComponent={Header} size="large" {...rest}>
      <ArticlePreview
        article={article || formValuesToData(values)}
        livePreview
      />
    </Modal>
  )
}

export default ArticlePreviewModal
