/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'

import { H2 } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import Authorize from 'pubsweet-client/src/helpers/AuthorizeWithGraphQL'

import ComposedEditorPanel from './compose/EditorPanel'
import Loading from './Loading'

import {
  getDecision,
  isAccepted,
  isApprovedByScienceOfficer,
  isNotApprovedByScienceOfficer,
  isRejected,
} from '../helpers/status'

import {
  DecisionSection,
  Discuss,
  EditorPanelMetadata,
  EditorPanelRibbon,
  PanelInfo,
  ReviewerInfo,
  ScienceOfficerSection,
} from './ui'

const Wrapper = styled.div`
  padding-right: calc(${th('gridUnit')} * 2);
`

const Header = styled(H2)`
  color: ${th('colorText')};
`

const EditorPanel = props => {
  const {
    article,
    currentUser,
    editor,
    editorSuggestedReviewers,
    loading,
    reviewerCounts,
    reviews,
    scienceOfficer,
    sendChat,
    updateArticle,
    updateMetadata,
  } = props

  if (loading) return <Loading />

  const { authors, status } = article
  const decision = getDecision(status)

  const author = authors.find(a => a.submittingAuthor)
  const alreadyRejected = isRejected(status)
  const approved = isApprovedByScienceOfficer(status)
  const notApproved = isNotApprovedByScienceOfficer(status)
  const scienceOfficerHasDecision = approved || notApproved

  const deriveRibbonStatus = () => {
    if (isAccepted(status)) return 'accepted'
    if (isRejected(status)) return 'rejected'
    if (decision === 'revise') return 'revise'
    if (approved) return 'scienceOfficerApproved'
    if (notApproved) return 'scienceOfficerDeclined'
    if (!scienceOfficerHasDecision) return 'scienceOfficerPending'
    return null
  }

  return (
    <Wrapper>
      <Header>Editor Panel</Header>

      {!loading && (
        <React.Fragment>
          <EditorPanelRibbon type={deriveRibbonStatus()} />

          <PanelInfo
            author={author}
            editor={editor}
            scienceOfficer={scienceOfficer}
          />

          {!alreadyRejected &&
            !decision && (
              <Authorize operation="isScienceOfficer" unauthorized={null}>
                <ScienceOfficerSection
                  article={article}
                  editorSuggestedReviewers={editorSuggestedReviewers}
                  updateArticle={updateArticle}
                />
              </Authorize>
            )}

          <Discuss
            article={article}
            currentUser={currentUser}
            sendChat={sendChat}
          />

          <ReviewerInfo
            articleId={article.id}
            reviewerCounts={reviewerCounts}
            reviews={reviews}
          />

          <DecisionSection article={article} updateArticle={updateArticle} />

          <EditorPanelMetadata
            articleId={article.id}
            doi={article.doi}
            updateMetadata={updateMetadata}
          />
        </React.Fragment>
      )}
    </Wrapper>
  )
}

const Composed = () => <ComposedEditorPanel render={EditorPanel} />
export default Composed
