/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { State } from 'react-powerplug'

import { Action as UIAction } from '@pubsweet/ui'
import {
  ArticlePreviewModal,
  AuthorSectionItem,
  EditorSectionItem,
  ReviewerSectionItem,
  Section,
} from './ui'

import ComposedDashboard from './compose/Dashboard'
import Loading from './Loading'
import { GET_DASHBOARD_ARTICLES } from './compose/pieces/getDashboardArticles'
import { CURRENT_USER } from './Private'

const SubmitButton = props => {
  const { client, createManuscript, currentUser, history } = props

  const onClick = () => {
    createManuscript().then(res => {
      const manuscriptId = res.data.createManuscript

      /* 
        TO DO -- This needs to go. See comment in mutation.
      */
      client
        .query({
          fetchPolicy: 'network-only',
          query: CURRENT_USER,
        })
        .then(() => {
          client.query({
            fetchPolicy: 'network-only',
            query: GET_DASHBOARD_ARTICLES,
            variables: { currentUserId: currentUser.id },
          })

          history.push(`/article/${manuscriptId}`)
        })
    })
  }

  return <Action onClick={onClick}>New Submission</Action>
}

const Action = styled(UIAction)`
  line-height: unset;
`

const DashboardWrapper = styled.div`
  margin: 0 auto;
  max-width: 1024px;
`

const Dashboard = props => {
  const {
    authorArticles,
    client,
    createManuscript,
    createReview,
    currentUser,
    deleteArticle,
    editorArticles,
    globalEditorTeam,
    handleInvitation,
    history,
    isGlobal,
    loading,
    reviewerArticles,
  } = props

  if (loading) return <Loading />

  const headerActions = [
    <SubmitButton
      client={client}
      createManuscript={createManuscript}
      currentUser={currentUser}
      history={history}
      key="createManuscript"
    />,
  ]

  const respondToInvitation = (articleId, action) => {
    const variables = {
      action,
      articleId,
      currentUserId: currentUser.id,
    }

    handleInvitation({ variables }).then(res => {
      if (action === 'accept') {
        const reviewInput = {
          articleVersionId: articleId,
          reviewerId: currentUser.id,
        }

        createReview({ variables: { input: reviewInput } })
      }
    })
  }

  return (
    <State initial={{ previewData: null, showModal: false }}>
      {({ state, setState }) => {
        const { previewData, showModal } = state

        const openModal = article =>
          setState({
            previewData: article,
            showModal: true,
          })

        const closeModal = () =>
          setState({
            previewData: null,
            showModal: false,
          })

        const openReviewerPreviewModal = articleId => {
          const article = reviewerArticles.find(a => a.id === articleId)
          openModal(article)
        }

        return (
          <React.Fragment>
            <DashboardWrapper>
              <Section
                actions={headerActions}
                deleteArticle={deleteArticle}
                itemComponent={AuthorSectionItem}
                items={authorArticles}
                label="My Articles"
              />

              <Section
                handleInvitation={respondToInvitation}
                itemComponent={ReviewerSectionItem}
                items={reviewerArticles}
                label="Reviews"
                openReviewerPreviewModal={openReviewerPreviewModal}
              />

              {isGlobal && (
                <Section
                  editors={globalEditorTeam.members}
                  itemComponent={EditorSectionItem}
                  items={editorArticles}
                  label="Editor Section"
                />
              )}
            </DashboardWrapper>

            <ArticlePreviewModal
              article={previewData}
              isOpen={showModal}
              onRequestClose={closeModal}
            />
          </React.Fragment>
        )
      }}
    </State>
  )
}

const Composed = props => <ComposedDashboard render={Dashboard} {...props} />
export default Composed
