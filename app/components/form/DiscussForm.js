/* eslint-disable react/prop-types */

import React from 'react'

import { Form } from './index'

const initialValues = {
  content: '',
}

const DiscussForm = props => {
  const { article, sendChat, ...otherProps } = props

  const handleSubmit = (values, formikBag) => {
    const { content: message } = values
    const { id: manuscriptId } = article

    sendChat({
      variables: {
        input: {
          manuscriptId,
          message,
        },
      },
    })
  }

  return (
    <Form
      initialValues={initialValues}
      onSubmit={handleSubmit}
      {...otherProps}
    />
  )
}

export default DiscussForm
